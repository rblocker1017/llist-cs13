public class LList<X>
{
    Node<X> head;
    Node<X> last;
    private int size; // Keeps track of list size 
    
    public LList()
    {
        clear();
    }
    
    public void addLast(X s)
    {
        if (head == null)
        {
            head = new Node<X>(s, null);
            last = head;
        }
        else
        {
            Node<X> n = head;  // Start at the head
            while (n.next != null)  // As long as there is another Node
            {
                // Hop to the next one
                n = n.next;
            }
            // Now we are at the last Node
            n.next = new Node<X>(s, null);
            last = n;
        }
        size++;
    }
    
    public void addFirst(X s)
    {
        head = new Node<X>(s, head);
        size++;
    }
    
    public void add(X s) // compatibility method for Alf
    {
        addLast(s);
    }
    
    public int size()
    {
        return size;
    }
    
    public X get(int index)
    {
        X rv; // Return value
        Node<X> n = head;
        if(index > -1 && index < size)
        {
            for(int i = 0; i < index; i++)
            {
                n = n.next;
            }
            rv = n.data;
        }
        else
        {
            throw new Inception();
        }
        return rv;
    }
    
    public int indexOf(X target)
    {
        Node<X> n = head;
        int rv = 0;
        for(int i = 0; i < size; i++)
        {
            if ( n.data.equals(target) )
            {
                i = size;
            }
            else
            {
                n = n.next;
                rv++;
            }
        }
        if(rv == size)
        {
            rv = -1;
        }
        
        return rv;
    }
    
    public void set(int index, X s)
    {
        if(index > -1 && index < size)
        {
            Node n = head;   // Start at the head
            for (int i = 0; i < index; i++)  // Count as we go
            {
                n = n.next;  // Hop to the next Node
            }
            n.data = s;
        }
        else
        {
            throw new Inception();
        }
    }
    
    public void remove(int index)
    {
        if(index == 0 && index < size)
        {
            removeFirst();
        }
        else if(index > 0 && index < size)
        {
            Node<X> n = head;
            for(int i = 0; i < index - 1; i++)
            {
                n = n.next;
            }
            n.next = n.next.next;
        
            size--;
        }
        else
        {
            throw new Inception();
        }
    }
    
    public void removeFirst()
    {
        if(size > 0)
        {
            Node<X> n = head; 
            n = n.next;
            head = n;
            size--;
        }
        else
        {
            throw new Inception();
        }
    }
    
    public void removeLast()
    {
        if(size > 0)
        {
            last = null;
            size--;
        }
        else
        {
            throw new Inception();
        }
    }
    
    public void clear()
    {
        head = null;
    }
    
    public void insert(int index, X s)
    {
        if(index > 0 && index <= size)
        {
            Node<X> n = head; 
            for(int i = 0; i < index - 1; i++)
            {
                n = n.next;
            }
        
            Node<X> t = new Node<X>(s, n.next);
            n.next = t;
            size++;
        }
        else if(index == 0)
        {
            addFirst(s);
        }
        else
        {
            throw new Inception();
        }
    }

    public String toString()
    {
        String rv; // Return value
        Node<X> n = head;
        if(size > 0)
        {
            rv = "" + n.data;
        }
        else
        {
            rv = "";
        }
        
        for(int i = 1; i < size; i++)
        {
            n = n.next;
            rv = rv + ", " + n.data;
        }
        
        rv = "[" + rv + "]";
        return rv;
    }
}