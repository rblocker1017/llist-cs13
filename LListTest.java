import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class LListTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class LListTest
{
    /**
     * Default constructor for test class LListTest
     */
    public LListTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }
    
    @Test
    public void testFirstAdd()
    {
        LList x = new LList();
        assertEquals(null, x.head);
        
        x.addLast("Werd");
        assertEquals("Werd", x.head.data);
        assertEquals(null, x.head.next);
    }
    
    @Test
    public void testTwoAdds()
    {
        LList x = new LList();
        x.addLast("Werd");
        x.addLast("Xenix");
        assertEquals("Werd", x.head.data);
        assertEquals("Xenix", x.head.next.data);
        assertEquals(null, x.head.next.next);
    }
    
    @Test
    public void testFourAdds()
    {
        LList x = new LList();
        x.addLast("Werd");
        x.addLast("Xenix");
        x.addLast("Zen");
        x.addLast("Bird");
        assertEquals("Werd", x.head.data);
        assertEquals("Xenix", x.head.next.data);
        assertEquals("Zen", x.head.next.next.data);
        assertEquals("Bird", x.head.next.next.next.data);
        assertEquals(null, x.head.next.next.next.next);
    }
    
    @Test
    public void testAddFirst()
    {
        LList x = new LList();
        x.addLast("Werd");
        x.addLast("Xenix");
        x.addLast("Zen");
        x.addFirst("First!");
        assertEquals("First!", x.head.data);
        assertEquals("Werd", x.head.next.data);
        assertEquals("Xenix", x.head.next.next.data);
    }
    
    @Test
    public void testAddFirst2()
    {
        LList x = new LList();
        x.addFirst("First!");
        assertEquals("First!", x.head.data);
        assertEquals(null, x.head.next);
        
        x.addFirst("Really first");
        assertEquals("Really first", x.head.data);
        assertEquals("First!", x.head.next.data);
    }
    
    @Test
    public void testSet()
    {
        LList x = new LList();
        x.addLast("Werd");
        x.addLast("Xenix");
        x.addLast("Zen");
        x.addFirst("First!");
        x.set(2, "Third");
        assertEquals("Third", x.head.next.next.data);
    }
    
    @Test
    public void testIndexOf()
    {
        LList x = new LList();
        x.addLast("Werd");
        x.addLast("Xenix");
        x.addLast("Zen");
        assertEquals(0, x.indexOf( "Werd" ));
    }
   
    @Test
    public void testIndexOf2()
    {
        LList x = new LList();
        x.addLast("one");
        x.addLast("two");
        x.addLast("three");
        x.addLast("four");
        assertEquals(2, x.indexOf("three"));
    }
    @Test
    public void testSize()
    {
        LList x = new LList();
        x.addLast("one");
        x.addLast("two");
        x.addLast("three");
        x.addLast("four");
        assertEquals(4, x.size());
    }
    
    @Test
    public void testToString()
    {
        LList x = new LList();
        x.addLast("one");
        x.addLast("two");
        x.addLast("three");
        x.addLast("four");
        assertEquals("[one, two, three, four]", x.toString());
    }
    
    @Test
    public void testGet()
    {
        LList x = new LList();
        x.addLast("one");
        x.addLast("two");
        x.addLast("three");
        x.addLast("four");
        assertEquals("four", x.get(3));
    }
    
    @Test
    public void testIndexOfNegative()
    {
        LList x = new LList();
        x.addLast("one");
        x.addLast("two");
        x.addLast("three");
        x.addLast("four");
        assertEquals( -1, x.indexOf("five"));
    }
    
    @Test
    public void testInsert()
    {
        LList x = new LList();
        x.addLast("one");
        x.addLast("two");
        x.addLast("three");
        x.insert(1, "five");
        assertEquals("[one, five, two, three]", x.toString());
    }
    
    @Test
    public void testInsert2()
    {
        LList x = new LList();
        x.addLast("one");
        x.addLast("two");
        x.addLast("three");
        x.addLast("five");
        x.addLast("six");
        x.addLast("seven");
        x.insert( 2 ,"four");
        assertEquals("[one, two, four, three, five, six, seven]", x.toString());
    }
    
    @Test
    public void testRemove()
    {
        LList x = new LList();
        x.addLast("one");
        x.addLast("two");
        x.addLast("three");
        x.addLast("five");
        x.remove(2);
        assertEquals("[one, two, five]", x.toString());
        
    }
    
    @Test
    public void testRemoveFirst()
    {
        LList x = new LList();
        x.addLast("one");
        x.addLast("two");
        x.addLast("three");
        x.addLast("five");
        x.removeFirst();
        assertEquals("[two, three, five]", x.toString());
    }
    
    @Test
    public void testRemoveLast()
    {
        LList x = new LList();
        x.addLast("one");
        x.addLast("two");
        x.addLast("three");
        x.addLast("five");
        x.removeLast();
        assertEquals("[one, two, three]", x.toString());
    }
    @Test
    public void testCreateList()
    {
        LList<Integer> b = new LList<Integer>();
        assertEquals(0, b.size());
    }
    
    @Test
    public void testAddToList()
    {
        LList<String> b = new LList<String>();
        b.add("Hello");
        assertEquals(1, b.size());
        b.add("Bye");
        assertEquals(2, b.size());
        b.add("Adios");
        assertEquals(3, b.size());
    }
    
    @Test
    public void testAdd12()
    {
        LList<String> b = new LList<String>();
        b.add("str1");
        b.add("str2");
        b.add("str3");
        b.add("str4");
        b.add("str5");
        b.add("str6");
        b.add("str7");
        b.add("str8");
        b.add("str9");
        b.add("str10");
        b.add("str11");
        b.add("str12");
        assertEquals(12, b.size());
    }
    
    @Test
    public void testAdd100()
    {
        LList<String> b = new LList<String>();
    
        for (int i = 0; i < 50; i++)
            b.add("str" + i);
        assertEquals(50, b.size());
    
        for (int i = 0; i < 50; i++)
            b.add("morestr" + i);
        
        assertEquals(100, b.size());
    }

    @Test
    public void testAddFirst1()
    {
        LList<String> a = new LList<String>();
        a.addFirst("str0");
        assertEquals("str0", a.get(0));
        a.addFirst("str1");
        assertEquals("str1", a.get(0));
        a.addFirst("str2");
        assertEquals("str2", a.get(0));
        assertEquals("str1", a.get(1));
        assertEquals("str0", a.get(2));
    }
   

    @Test
    public void testGet1()
    {
        LList<String> b = new LList<String>();
        b.add("Hello");
        b.add("Bye");
        assertEquals("Hello", b.get(0));
        assertEquals("Bye", b.get(1));
    }
    
    @Test (expected=Inception.class)
    public void testGetException()
    {
        LList<String> b = new LList<String>();
        b.add("Hello");
        b.get(1);
        // No more code after this, because the exception
        // will cause the method to exit.
        // Don't create more that one exception per test method.
    }
    
    @Test (expected=Inception.class)
    public void testGetNegativeException()
    {
        LList b = new LList();
        b.get(-1);
    }
    
    @Test
    public void testGet12()
    {
        LList<String> b = new LList<String>();
        b.add("str1");
        b.add("str2");
        b.add("str3");
        b.add("str4");
        b.add("str5");
        b.add("str6");
        b.add("str7");
        b.add("str8");
        b.add("str9");
        b.add("str10");
        b.add("str11");
        b.add("str12");
        assertEquals("str8", b.get(7));
        assertEquals("str12", b.get(11));
        assertEquals("str1", b.get(0));
    }

    @Test
    public void testSet1()
    {
        LList<String> b = new LList<String>();
        b.add("Hello");
        b.add("Bye");
        b.set(0, "Bonjour");
        assertEquals("Bonjour", b.get(0));
        assertEquals("Bye", b.get(1));
        b.set(1, "Adios");
        assertEquals("Bonjour", b.get(0));
        assertEquals("Adios", b.get(1));
    }
    
    @Test (expected=Inception.class)
    public void testSetException()
    {
        LList<String> b = new LList<String>();
        b.add("Hello");
        b.set(1, "Bye");
        // No more code after this, because the exception
        // will cause the method to exit.
        // Don't create more that one exception per test method.
    }
    
    @Test (expected=Inception.class)
    public void testSetEmptyException()
    {
        LList<String> b = new LList<String>();
        b.set(0, "Hello");
    }

    @Test (expected=Inception.class)
    public void testSetNegativeException()
    {
        LList<String> b = new LList<String>();
        b.set(-1, "Hello");
    }

    @Test (expected=Inception.class)
    public void testSetFullException()
    {
        LList<String> b = new LList<String>();
        b.add("Hello");
        b.add("Bye");
        b.set(2, "Bonjour");
    }
    
    @Test
    public void testToString1()
    {
        LList<String> b = new LList<String>();
        assertEquals("[]", b.toString());
        b.add("hello");
        assertEquals("[hello]", b.toString());
        b.add("bye");
        assertEquals("[hello, bye]", b.toString());
        b.add("adios");
        assertEquals("[hello, bye, adios]", b.toString());
    }
    
    @Test
    public void testRemoveLast1()
    {
        LList<String> b = new LList<String>();
        b.add("hello");
        b.add("hola");
        b.removeLast();
        assertEquals(1, b.size());
        assertEquals("hello", b.get(0));
        b.removeLast();
        assertEquals(0, b.size());
    }
	
    @Test
    public void testRemoveFirst1()
    {
        LList<String> b = new LList<String>();
        b.add("hello");
        b.add("hola");
        b.removeFirst();
        assertEquals(1, b.size());
        assertEquals("hola", b.get(0));
        b.removeFirst();
        assertEquals(0, b.size());
    }
    
    @Test
    public void testRemoveLast10()
    {
        LList<String> b = new LList<String>();
        for (int i = 0; i < 10; i++)
            b.add("str" + i);
        assertEquals(10, b.size());
        b.removeLast();
        assertEquals(9, b.size());
        b.removeLast();
        b.removeLast();
        b.removeLast();
        b.removeLast();
        b.removeLast();
        b.removeLast();
        b.removeLast();
        b.removeLast();
        b.removeLast();
        assertEquals(0, b.size());
    }
    
    @Test (expected=Inception.class)
    public void testRemoveException()
    {
        LList<String> b = new LList<String>();
        b.add("hello");
        b.removeLast();
        b.removeLast();
    }
   
    @Test
    public void testInsert1()
    {
        LList<String> b = new LList<String>();
        b.add("hello");
        b.add("bye");
        b.insert(1, "adios");
        assertEquals("hello", b.get(0));
        assertEquals("adios", b.get(1));
        assertEquals("bye", b.get(2));
        assertEquals(3, b.size());
        
        b.insert(1, "buenos dias");
        assertEquals(4, b.size());
        assertEquals("hello", b.get(0));
        assertEquals("buenos dias", b.get(1));
        assertEquals("adios", b.get(2));
        
        b.insert(0, "hi");
        assertEquals(5, b.size());
        assertEquals("hi", b.get(0));
        assertEquals("hello", b.get(1));
        
        b.insert(4, "see ya");
        assertEquals(6, b.size());
        assertEquals("see ya", b.get(4));
        assertEquals("bye", b.get(5));
    }
    
    @Test
    public void testInsert11At0()
    {
        LList<String> b = new LList<String>();
        b.add("one");
        b.insert(0, "two");
        b.insert(0, "three");
        b.insert(0, "four");
        b.insert(0, "five");
        b.insert(0, "six");
        b.insert(0, "seven");
        b.insert(0, "eight");
        b.insert(0, "nine");
        b.insert(0, "ten");
        b.insert(0, "eleven");
        assertEquals(11, b.size());
        assertEquals("eleven", b.get(0));
        assertEquals("ten", b.get(1));
        assertEquals("two", b.get(9));
        assertEquals("one", b.get(10));
    }
    
    @Test
    public void testInsertAt0()
    {
        LList<String> b = new LList<String>();
        b.insert(0, "one");
        b.insert(0, "two");
        b.insert(0, "three");
        assertEquals(3, b.size());
        assertEquals("three", b.get(0));
        assertEquals("two", b.get(1));
        assertEquals("one", b.get(2));
    }
    
    @Test (expected=Inception.class)
    public void testInsertException()
    {
        LList<String> b = new LList<String>();
        b.add("hello");
        b.insert(2, "bye");
    }
    
    @Test (expected=Inception.class)
    public void testInsertNegative()
    {
        LList<String> b = new LList<String>();
        b.insert(-1, "hello");
    }


    @Test
    public void testRemove1()
    {
        LList<String> b = new LList<String>();
        b.add("hello");
        b.remove(0);
        assertEquals(0, b.size());
    }
    
    @Test
    public void testRemoveSeveral()
    {
        LList<String> b = new LList<String>();
        b.add("hello");
        b.add("bye");
        b.add("adios");
        b.add("ciao");
        b.add("see ya");
        b.remove(0);
        assertEquals(4, b.size());
        assertEquals("bye", b.get(0));
        assertEquals("adios", b.get(1));
        assertEquals("ciao", b.get(2));
        assertEquals("see ya", b.get(3));
        
        b.remove(3);
        assertEquals(3, b.size());
        assertEquals("bye", b.get(0));
        assertEquals("adios", b.get(1));
        assertEquals("ciao", b.get(2));
        
        b.remove(0);
        b.remove(0);
        b.remove(0);
        assertEquals(0, b.size());
    }
    
    @Test (expected=Inception.class)
    public void testDeleteException()
    {
        LList<String> b = new LList<String>();
        b.add("hello");
        b.remove(1);
    }
    
    @Test (expected=Inception.class)
    public void testRemoveNegative()
    {
        LList<String> b = new LList<String>();
        b.remove(-1);
    }
    
    @Test (expected=Inception.class)
    public void testRemoveEmpty()
    {
        LList<String> b = new LList<String>();
        b.add("hello");
        b.remove(0);
        b.remove(0);
    }

    @Test
    public void testIndexOf1()
    {
        LList<String> b = new LList<String>();
        
        for (int i = 0; i < 20; i++)
            b.add("str" + i);
        
        assertEquals(0, b.indexOf("str0"));
        assertEquals(19, b.indexOf("str19"));
        assertEquals(-1, b.indexOf("not found"));
    }
    
    @Test
    public void testIndexOfWithDuplicates()
    {
        LList<String> b = new LList<String>();
        b.add("str1");
        b.add("str2");
        b.add("str3");
        b.add("str2");
        b.add("str3");
        
        assertEquals(0, b.indexOf("str1"));
        assertEquals(1, b.indexOf("str2"));
        assertEquals(2, b.indexOf("str3"));
    }

    
    // Make sure LList works with different kinds
    // of objects.
    
    @Test
    public void testInteger()
    {
        LList<Integer> b = new LList<Integer>();
        for (int i = 0; i < 50; i++)
            b.add(i);
        assertEquals(50, b.size());
        assertEquals(0, (int)b.get(0));
        assertEquals(1, (int)b.get(1));
        assertEquals(49, (int)b.get(49));
    }
    
    @Test
    public void testJFrame()
    {
        LList<javax.swing.JFrame> b = new LList<javax.swing.JFrame>();
        javax.swing.JFrame j1 = new javax.swing.JFrame();
        javax.swing.JFrame j2 = new javax.swing.JFrame();
        b.add(j2);
        b.add(j1);
        assertEquals(j2, b.get(0));
        assertEquals(j1, b.get(1));
    }
    
    
    @Test
    public void testRigorous1()
    {
        LList<String> a = new LList<String>();
        a.addLast("str0");
        a.addFirst("str1");
        a.addLast("str2");
        a.addFirst("str3");
        a.addLast("str4");
        assertEquals("str3", a.get(0));
        assertEquals("str1", a.get(1));
        assertEquals("str0", a.get(2));
        assertEquals("str2", a.get(3));
        assertEquals("str4", a.get(4));
        
        a.removeFirst();
        assertEquals("str1", a.get(0));
        assertEquals("str0", a.get(1));
        assertEquals("str2", a.get(2));
        assertEquals("str4", a.get(3));
        
        a.removeLast();
        assertEquals("str1", a.get(0));
        assertEquals("str0", a.get(1));
        assertEquals("str2", a.get(2));
        
        a.removeFirst();
        assertEquals("str0", a.get(0));
        assertEquals("str2", a.get(1));
        
        a.removeLast();
        assertEquals("str0", a.get(0));
        
        a.removeLast();
        assertEquals(0, a.size());
    }

    @Test
    public void testRigorous2()
    {
        LList<Integer> a = new LList<Integer>();
        a.addLast(0);
        a.addFirst(1);
        a.insert(1, 2);
        a.insert(1, 3);
        a.insert(2, 4);
        a.remove(1);
        a.remove(0);
        a.insert(0, 5);
        a.remove(3);
        a.insert(2, 6);
        a.addLast(7);
        a.addFirst(8);
        a.removeLast();
        a.removeFirst();
        a.addFirst(9);
        a.removeLast();
        a.set(3, 10);
        a.set(0, 11);
        // I hope I got this right! -BB
        assertEquals("[11, 5, 4, 10]", a.toString());
        a.remove(0);
        a.remove(0);
        assertEquals("[4, 10]", a.toString());
        a.removeLast();
        assertEquals("[4]", a.toString());
    }
}